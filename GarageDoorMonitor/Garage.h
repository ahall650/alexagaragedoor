//
//  Garage.h
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Garage : NSObject
@property (nonatomic, strong)NSString *doorStatus;
@property (nonatomic, strong)NSArray *history;
-(void)setDoorStatus;
-(void)openDoor;
-(void)closeDoor;
@end
