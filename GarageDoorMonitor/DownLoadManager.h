//
//  DownLoadManager.h
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DownLoadManager : NSObject
+ (DownLoadManager*)SharedDownLoadManager;
-(void)downloadData;
@end
