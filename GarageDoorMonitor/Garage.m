//
//  Garage.m
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "Garage.h"
#import "PhotonController.h"
//#import "DownLoadManager.h"
@interface Garage()
@property (nonatomic, strong)PhotonController *photonController;
@end

@implementation Garage
- (id)init {
    self = [super init];
    
    if(self) {
        
        _photonController = [PhotonController SharedPhotonController];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(setDoorStatus)
                                                     name:@"newStatusNotification"
                                                   object:nil];
    }
    return self;
}
-(void)setDoorStatus{
    NSLog(@"garage status updated");
    _doorStatus = _photonController.doorStatus;
    [self postNewStatus];
}
-(void)postNewStatus{
    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"garageDidUpdate" object:nil];
    });
}

-(void)openDoor{
    [_photonController toggleDoorWithCommand:@"open"];
}
-(void)closeDoor{
    [_photonController toggleDoorWithCommand:@"close"];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
@end
