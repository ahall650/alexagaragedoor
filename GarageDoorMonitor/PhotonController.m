//
//  PhotonController.m
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "PhotonController.h"
#import "Spark-SDK.h"
#import "DownLoadManager.h"
#define USER   @"parkerhall88@yahoo.com"
#define PASS   @"AAnt651!"

@interface PhotonController()
@property (nonatomic, strong)SparkDevice *thePhoton;
@property (nonatomic, strong)SparkCloud *theSession;
@property (nonatomic, strong)NSString *token;
@property (nonatomic, strong)id ActivityListener;
@end


@implementation PhotonController
+ (PhotonController *)SharedPhotonController
{
    static PhotonController *_SharedPhotonController = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _SharedPhotonController = [PhotonController new];
    });
    
    return _SharedPhotonController;
}

- (id)init {
    self = [super init];
    
    if(self) {
        NSLog(@"init code");
        [self login];
        [self getDevice];
    }
    return self;
}
- (void)login{
    // logging in
    NSLog(@"logging in");
    _theSession = [SparkCloud sharedInstance];
    
    [_theSession loginWithUser:USER password:PASS completion:^(NSError *error) {
        if (!error)
        {
            _token = _theSession.accessToken;
            //[self testing];
        }
        else
            NSLog(@"Wrong credentials or no internet connectivity, please try again");
    }];
    
}
-(void)getDevice{
    __block SparkDevice *myPhoton;
    [[SparkCloud sharedInstance] getDevices:^(NSArray *sparkDevices, NSError *error) {
        // search for a specific device by name
        for (SparkDevice *device in sparkDevices)
        {
            if ([device.name isEqualToString:@"Rouge_One"])
                myPhoton = device;
            _thePhoton = device;
        }
        [self subscribeToParticleEvents];

        [self getDoorStatus];

    }];
}
-(void)getDoorStatus{
    [_thePhoton getVariable:@"doorPosition" completion:^(id result, NSError *error) {
        if (!error)
        {
            NSLog(@"The result is: %@",result);
            _doorStatus = result;
            [self postNewStatus];
        }
        else
        {
            NSLog(@"%@",error);
        }
    }];
}
-(void)postNewStatus{
    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"newStatusNotification" object:nil];
    });
}

- (void)toggleDoorWithCommand:(NSString *)command{
    // calling a function
    if ([_theSession injectSessionAccessToken:_token]){
        NSLog(@"Session is active!");
    }
    else{
        NSLog(@"Bad access token provided");
    }
    
    [_thePhoton callFunction:@"toggleDoor" withArguments:@[command] completion:^(NSNumber *resultCode, NSError *error) {
        if (!error)
        {
            if([resultCode isEqual:@1]){
                NSLog(@"door was %@",command);
            }else{
                NSLog(@"door is already %@",command);
            }
        }else{
            NSLog(@"the Error is: %@ ", error);
        }
        NSLog(@"result code is: %@",resultCode);

        
    }];
}

-(void)subscribeToParticleEvents{
    // The event handler:
    SparkEventHandler handler = ^(SparkEvent *event, NSError *error) {
        if (!error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Got Event %@ with data: %@",event.event,event.data);
                [self getDoorStatus];
            });
        }
        else
        {
            NSLog(@"Error occured: %@",error.localizedDescription);
        }
        
    };
    NSLog(@"subscribed");
    //_ActivityListener = [_thePhoton subscribeToEventsWithPrefix:@"thePosition" handler:handler];
    _ActivityListener = [_thePhoton subscribeToEventsWithPrefix:@"ios" handler:handler];
    //NSLog(@"Listener: %@",self.ActivityListener);


}
@end
