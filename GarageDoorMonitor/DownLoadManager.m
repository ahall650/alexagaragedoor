//
//  DownLoadManager.m
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "DownLoadManager.h"
#import "AFNetworking.h"

@interface DownLoadManager()


@end
static NSString *phantURL = @"http://data.sparkfun.com/output/LQvrEjN4ADfAmOWAGWdA.json";

@implementation DownLoadManager
+ (DownLoadManager *)SharedDownLoadManager
{
    static DownLoadManager *_SharedDownLoadManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _SharedDownLoadManager = [DownLoadManager new];
    });
    
    return _SharedDownLoadManager;
}

- (id)init {
    self = [super init];
    
    if(self) {
        

    }
    return self;
}
-(void)downloadData{
    NSLog(@"manager called");
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:phantURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"THE RESPONSE FROM SERVER: %@ %@", response, responseObject);
        }
    }];
    [dataTask resume];
}
@end
