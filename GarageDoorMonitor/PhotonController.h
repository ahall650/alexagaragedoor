//
//  PhotonController.h
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotonController : NSObject
+ (PhotonController*)SharedPhotonController;

- (void)toggleDoorWithCommand:(NSString *)command;


@property (nonatomic,strong)NSString *doorStatus;
@end
