//
//  ViewController.m
//  GarageDoorMonitor
//
//  Created by anthony hall on 1/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "ViewController.h"
#import "PhotonController.h"
#import "DownLoadManager.h"
#import "Garage.h"
@interface ViewController ()
@property (nonatomic, strong)PhotonController *photonController;
@property (nonatomic, strong)Garage *garage;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateStatusLabel)
                                                 name:@"garageDidUpdate"
                                               object:nil];
    _garage = [Garage new];
    [self styleViews];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateStatusLabel{
    NSLog(@"update triggered");
    _positionLabel.text = _garage.doorStatus;
}
-(void)styleViews{
    _positionLabel.text = _photonController.doorStatus;
    _headerLabel.font = [_headerLabel.font fontWithSize:50];
    _positionLabel.font = [_positionLabel.font fontWithSize:50];
    _headerLabel.text = @"Door is:";
    _positionLabel.text = @"";
    [_openButton setTitle:@"Open" forState:UIControlStateNormal];
    [_closeButton setTitle:@"Close" forState:UIControlStateNormal];
}
-(void)downloadData{
    [[DownLoadManager SharedDownLoadManager]downloadData];

}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}
- (IBAction)openDoor:(id)sender {
    [_garage openDoor];
}

- (IBAction)closeDoor:(id)sender {
    [_garage closeDoor];
}
@end
